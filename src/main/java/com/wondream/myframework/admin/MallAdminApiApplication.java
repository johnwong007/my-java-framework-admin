package com.wondream.myframework.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wondream.myframework.admin.dao")
public class MallAdminApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallAdminApiApplication.class, args);
    }

}
